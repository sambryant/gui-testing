import pytest
import random
#import lib.abstractpixbuf
from lib.abstractpixbuf import AbstractPixBuf



def run_size_limit_test(size, lim_size, exp_size, scale_up):
  result_size = AbstractPixBuf.size_limits_to_size(size, lim_size, scale_up)
  assert result_size == exp_size

def test_size_limits_too_small_same_ratio():
  # Even ratio for act/desired
  run_size_limit_test((30, 30), (50, 50), (30, 30), False)
  run_size_limit_test((50, 30), (50, 50), (50, 30), False)
  run_size_limit_test((30, 50), (50, 50), (30, 50), False)
  run_size_limit_test((30, 30), (50, 50), (50, 50), True)
  run_size_limit_test((50, 30), (50, 50), (50, 30), True)
  run_size_limit_test((30, 50), (50, 50), (30, 50), True)

def test_size_limits_too_small_uneven_ratio():
  # Same ratio for act/desired
  run_size_limit_test((30, 20), (60, 40), (30, 20), False)
  run_size_limit_test((60, 20), (60, 40), (60, 20), False)
  run_size_limit_test((30, 40), (60, 40), (30, 40), False)
  run_size_limit_test((30, 20), (60, 40), (60, 40), True)
  run_size_limit_test((60, 20), (60, 40), (60, 20), True)
  run_size_limit_test((30, 40), (60, 40), (30, 40), True)

def test_size_limits_too_small_mixed_ratio():
  # Mixed ratios
  run_size_limit_test((30, 20), (60, 90), (60, 40), True)
  # Very close
  run_size_limit_test((30, 20), (40, 60), (40, 27), True)
  run_size_limit_test((30, 20), (40, 27), (40, 27), True)
  run_size_limit_test((30, 20), (40, 26), (39, 26), True)

  run_size_limit_test((40, 26), (60, 40), (60, 39), True)

def test_size_limits_too_big():
  # Even ratio for act/desired
  run_size_limit_test((50, 50), (30, 30), (30, 30), True)
  run_size_limit_test((50, 30), (30, 30), (30, 18), True)
  run_size_limit_test((30, 50), (30, 30), (18, 30), True)

def test_size_limits_too_big_uneven_ratio():
  # Same ratio for act/desired
  run_size_limit_test((60, 40), (30, 20), (30, 20), True)
  run_size_limit_test((60, 40), (60, 20), (30, 20), True)
  run_size_limit_test((40, 60), (20, 60), (20, 30), True)
  run_size_limit_test((80, 40), (40, 40), (40, 20), True)
  run_size_limit_test((80, 20), (60, 40), (60, 15), True)

def test_size_limits_rules():
  lim = AbstractPixBuf.size_limits_to_size
  a = (60, 40)
  b = (40, 40)
  c = lim(a, b, True)

  assert c == (40, 27)
  assert lim(c, b, True) == (40, 27)
  # assert lim(a, c, True) == (40, 27)

def test_size_limits_rules2():
  lim = AbstractPixBuf.size_limits_to_size
  a = (88, 469)
  b = (119, 429)
  c = (80, 429)

  assert lim(a, b, True) == c
  assert lim(c, b, True) == c
  # assert lim(a, c, True) == c

def assert_relns(a, b, c):
  lim = AbstractPixBuf.size_limits_to_size
  # rule 1: c <= b
  assert c[0] <= b[0] and c[1] <= b[1]

  # rule 2: c is a limit of b
  assert c[0] == b[0] or c[1] == b[1]

  # rule 3a: stability: if limit(a, b) = c, then limit(c, b) = c
  assert lim(c, b, True) == c
  # assert lim(a, c, True) == c

def test_size_limits_random():
  ntrials = 1000
  maxval = 500
  minval = 5
  for i in range(ntrials):
    w = int((maxval-minval)*random.random() + minval)
    h = int((maxval-minval)*random.random() + minval)
    dw = int((maxval-minval)*random.random() + minval)
    dh = int((maxval-minval)*random.random() + minval)

    a = (w, h)
    b = (dw, dh)
    c = AbstractPixBuf.size_limits_to_size(a, b, True)

    assert_relns(a, b, c)

    assert c[0] > 0 and c[1] > 0

    # assert c == AbstractPixBuf.size_limits_to_size(c, b, True)

    # # rule 3b: stability: if limit(a, b) = c, then limit(a, c) = c
    # assert c == AbstractPixBuf.size_limits_to_size(a, c, True)

    # rule 3: inverse: if limit(a, b) = c, then limit(c, a) = a
    #assert a == AbstractPixBuf.size_limits_to_size(c, a, True)
