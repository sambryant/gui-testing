import gi
import argparse
import abc
import os
import sys
import logging
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

class AbstractPixBuf(object):
  """
  Object representing a pixbuf that may or may not have been loaded yet.

  This is separate so we can treat unloaded and loaded pixbufs the same way.

  It can also cache resized or otherwise modified versions.
  """
  def __init__(self, file, scale_method=GdkPixbuf.InterpType.BILINEAR):
    self.file = file
    self.orig = None
    self.orig_size = None
    self.pix_cache = {}
    self.scale_method = scale_method
    self.cache_lock = threading.Lock()

  def __repr__(self):
    return self.file

  @staticmethod
  def size_limits_to_size(size, size_limit, scale_up):
    """
    Converts a size limit to the size this image would have to be to fit that limit while maintaining its regular aspect ratio.

    Returns a tuple
    """
    w, h = size
    dw, dh = size_limit

    if w == 0 or h == 0 or dw == 0 or dh == 0:
      raise Exception('Encountered zero dimension')

    if not scale_up and w <= dw and h <= dh:
      return size

    aspect0 = w / h
    nw1, nh1 = dw, max(1, min(dh, int(h*dw/w+0.5)))
    nw2, nh2 = max(1, min(dw, int(w*dh/h+0.5))), dh
    aspect1 = nw1/nh1
    aspect2 = nw2/nh2

    tst1 = abs((aspect1-aspect0)/aspect0)
    tst2 = abs((aspect2-aspect0)/aspect0)

    if tst1 < tst2:
      return (nw1, nh1)
    else:
      return (nw2, nh2)

  def _load_orig(self):
    self.orig = GdkPixbuf.Pixbuf.new_from_file(self.file)
    self.orig_size = (self.orig.get_width(), self.orig.get_height())
    self.pix_cache[self.orig_size] = self.orig

  def get(self, size_limit=None, scale_up=True):
    self.cache_lock.acquire()

    # make sure original has been loaded at some point.
    if not self.orig:
      self._load_orig()

    if not size_limit:
      return self.orig

    # convert a size limit to the size of this image within the limit that
    # preserves the aspect ratio
    size = AbstractPixBuf.size_limits_to_size(
      self.orig_size, size_limit, scale_up)

    # check if we have cached a scaled version of this image
    scaled = self.pix_cache.get(size, None)

    if not scaled:
      scaled = GdkPixbuf.Pixbuf.new(
        self.orig.get_colorspace(),
        self.orig.get_has_alpha(),
        self.orig.get_bits_per_sample(),
        size[0],
        size[1])

      scalex = (size[0] + 0.1)/self.orig.get_width()
      scaley = (size[1] + 0.1)/self.orig.get_height()

      self.orig.scale(scaled, 0, 0, size[0], size[1], 0, 0, scalex, scaley, self.scale_method)
      self.pix_cache[size] = scaled

    self.cache_lock.release()

    return scaled


  def clear_size_cache(self, size_limit_exceptions=None):
    size_limit_exceptions = size_limit_exceptions or []
    # Make sure that the original size key is not erased.
    raise Exception('Not implemented yet')
